module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "gumroad-red": "#da3a2f",
        "gumroad-yellow": "#f6d488",
        "gumroad-green": "#0f686b",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
