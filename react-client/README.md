# Gumroad Coding Challenge - v2

This is the refactored React app of the [MVP](https://gitlab.com/hansy/test-project/-/tree/main/client).

[View app](https://gumroad-coding-challenge-v2.vercel.app)

## Running locally

### Requirements

- Node.js
- [Pusher](https://pusher.com) account

### Installation

```bash
# install dependencies
$ yarn

# start server
$ yarn start
```

## Resources

To make development speedy, I used a few 3rd party libraries/frameworks, including:

### Tailwind CSS / Tailwind UI

- [Tailwind CSS](https://tailwindcss.com/)
- [TailwindUI](https://tailwindui.com/)

I'm a big fan of utility-based CSS frameworks where you construct your CSS components via stringing together utility classes (e.g. flex, background color, etc). Tailwind UI has some out-of-the-box UIs for components like product lists, so I grabbed some simple components and modified the CSS as needed.

### React Hook Form

- [React Hook Form](https://react-hook-form.com/)

Hooking up all the bits and pieces of forms (e.g. validations, error states/messages, submission) is a mind-numbingly tedious process. React Hook Forms makes this process so much easier and nicer.

### Pusher

- [Pusher](https://pusher.com) account

I deferred to a 3rd party solution for real-time client updates. This solution does rely on going through a server, which is lame considering there are ways to interface with the DB client-side only.
