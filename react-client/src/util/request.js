import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE,
  headers: { "Content-Type": "application/json" },
});

const request = async (url, method, data, headers) => {
  try {
    const res = await instance({
      method,
      url,
      data: JSON.stringify(data),
      headers: {
        ...headers,
      },
    });
    return res.data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const get = async (path, headers) => {
  return await request(path, "get", null, headers);
};

export const post = async (path, data, headers) => {
  return await request(path, "post", data, headers);
};
