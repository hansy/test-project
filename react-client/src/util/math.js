export const getPercent = (n, d) => {
  return ((n / d) * 100).toFixed(1);
};

// Returns average from array of numbers
export const getAverageFromArr = (arr) => {
  const total = arr.reduce(function (acc, val) {
    return acc + val;
  }, 0);

  return total / arr.length;
};

// Returns new average by adding a new value to old average
export const addToAverage = (oldAvg, newVal, newTotal) => {
  return oldAvg + (newVal - oldAvg) / newTotal;
};
