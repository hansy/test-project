export const displayPrice = (priceInCents) => {
  return `$${(priceInCents / 100).toFixed(2)}`;
};
