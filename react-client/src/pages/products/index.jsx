import React, { useEffect, useState } from "react";
import { trackPromise } from "react-promise-tracker";
import Container from "../../components/Container";
import ProductSnack from "../../components/Product/Snack";
import { getProducts } from "../../api/products";

const ProductsIndex = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const data = await getProducts();
        setProducts(data);
      } catch {
        // handle error
      }
    };
    trackPromise(fetchProducts());
  }, []);

  return (
    <Container>
      <h1 className="text-center text-3xl mb-4">Products</h1>
      <div className="-mx-px border-t border-l border-gray-200 grid grid-cols-2 sm:mx-0 md:grid-cols-3 lg:grid-cols-4">
        {products.map((product) => (
          <ProductSnack
            key={product.id}
            id={product.id}
            image={product.image}
            name={product.name}
            price={product.price}
          />
        ))}
      </div>
    </Container>
  );
};

export default ProductsIndex;
