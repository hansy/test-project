import React, { useState, useEffect, useCallback } from "react";
import { Link, useParams } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Container from "../../components/Container";
import Product from "../../components/Product";
import { getProduct } from "../../api/products";
import Pusher from "pusher-js";

const ProductPage = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({});
  const [reviews, setReviews] = useState([]);

  const addReview = useCallback(
    (review) => {
      setReviews([review, ...reviews]);
    },
    [reviews]
  );

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const data = await getProduct(id);
        const { reviews, ...product } = data;

        setProduct(product);
        setReviews(reviews);
      } catch {
        // handle error
      }
    };

    trackPromise(fetchProduct());
  }, [id]);

  useEffect(() => {
    const pusher = new Pusher(process.env.REACT_APP_PUSHER_APP_ID, {
      cluster: process.env.REACT_APP_PUSHER_CLUSTER,
    });
    const channel = pusher.subscribe("products");
    channel.bind("review", addReview);

    return () => {
      channel.unbind("review");
      pusher.unsubscribe("products");
    };
  }, [addReview]);

  return (
    <Container>
      <Link to="/">
        <span className="text-gray-700">&#8592; Back to products</span>
      </Link>
      <Product
        id={product.id}
        name={product.name}
        image={product.image}
        description={product.description}
        price={product.price}
        reviews={reviews}
        onReviewCreate={addReview}
      />
    </Container>
  );
};

export default ProductPage;
