import { post } from "../util/request";

export const createReview = (review) => {
  return post("/reviews", review);
};
