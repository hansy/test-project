import { get } from "../util/request";

export const getProducts = async () => {
  return get("/products");
};

export const getProduct = async (id) => {
  return get(`/products/${id}`);
};
