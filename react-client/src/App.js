import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Loader from "./components/Loader";
import ProductsIndex from "./pages/products/index";
import ProductPage from "./pages/products/_id";

const App = () => {
  return (
    <>
      <Loader />
      <Router>
        <Switch>
          <Route exact path="/" component={ProductsIndex} />
          <Route path="/products/:id" component={ProductPage} />
        </Switch>
      </Router>
    </>
  );
};

export default App;
