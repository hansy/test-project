import React, { useState } from "react";
import ProductHeader from "./Header";
import Reviews from "../Reviews";
import Modal from "../Modal";
import CreateReviewForm from "../Reviews/Review/CreateForm";

const Product = ({
  id,
  name,
  image,
  description,
  price,
  reviews,
  onReviewCreate,
}) => {
  const [modalOpen, setModalOpen] = useState(false);

  const handleNewReview = (review) => {
    setModalOpen(false);
    onReviewCreate(review);
  };

  return (
    <div>
      <Modal isOpen={modalOpen} onClose={() => setModalOpen(false)}>
        <CreateReviewForm
          productId={id}
          onSuccess={handleNewReview}
          onCancel={() => setModalOpen(false)}
        />
      </Modal>
      <ProductHeader
        name={name}
        image={image}
        description={description}
        price={price}
        reviews={reviews}
      />
      <hr />
      <div className="sm:flex items-center">
        <h2 className="text-2xl font-bold my-4">Customer Reviews</h2>
        <button
          className="ml-0 sm:ml-4 px-3 py-2 border border-gumroad-red text-gumroad-red rounded-md text-sm hover:bg-gumroad-red hover:text-white"
          type="button"
          onClick={() => setModalOpen(true)}
        >
          + Add review
        </button>
      </div>

      <Reviews reviews={reviews} />
    </div>
  );
};

export default Product;
