import { Link } from "react-router-dom";
import { displayPrice } from "../../../util/money";

const ProductSnack = ({ id, image, name, price }) => {
  return (
    <div
      key={id}
      className="group relative p-4 border-r border-b border-gray-200 sm:p-6"
    >
      <div
        className="bg-gray-200 group-hover:opacity-75 bg-cover bg-no-repeat bg-center w-full h-32"
        style={{ backgroundImage: `url('${image}')` }}
      ></div>
      <div className="pt-10 pb-4 text-center">
        <h3 className="text-sm font-medium text-gray-900">
          <Link to={`/products/${id}`}>
            <span aria-hidden="true" className="absolute inset-0" />
            {name}
          </Link>
        </h3>
        <p className="mt-4 text-base font-medium text-gray-900">
          {displayPrice(price)}
        </p>
      </div>
    </div>
  );
};

export default ProductSnack;
