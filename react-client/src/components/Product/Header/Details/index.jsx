import { displayPrice } from "../../../../util/money";
import StarRating from "../../../StarRating";
import { getAverageFromArr } from "../../../../util/math";

const ProductHeaderDetails = ({ name, description, price, reviews = [] }) => {
  const numReviews = reviews.length;
  const averageRating = () => {
    const ratings = reviews.map((r) => r.rating);
    return getAverageFromArr(ratings);
  };

  return (
    <div className="flex flex-col justify-between h-full">
      <div>
        <h1 className="text-2xl font-extrabold tracking-tight text-gray-900 sm:text-3xl">
          {name}
        </h1>

        <div className="my-1 flex items-center">
          <div>
            <h3 className="sr-only">Reviews</h3>
            <div className="flex items-center">
              <StarRating rating={averageRating()} />
            </div>
          </div>
          <span className="ml-3 text-gray-500">{numReviews} reviews</span>
        </div>
        <p className="text-gray-500 mt-6">{description}</p>
      </div>

      <button
        type="button"
        className="bg-gumroad-green border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base font-medium text-white hover:bg-gumroad-green focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-50 focus:ring-gumroad-green mt-5"
      >
        Buy for {displayPrice(price)}
      </button>
    </div>
  );
};

export default ProductHeaderDetails;
