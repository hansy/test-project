import ProductHeaderDetails from "./Details";

const ProductHeader = ({ name, image, description, price, reviews }) => {
  return (
    <div className="md:grid md:grid-cols-2 py-4">
      <div className=" md:col-span-1">
        <div
          className="bg-gray-200 bg-cover bg-no-repeat bg-center w-full h-72"
          style={{ backgroundImage: `url('${image}')` }}
        ></div>
      </div>
      <div className="mt-4 md:cols-span-1 md:pl-8 md:mt-0">
        <ProductHeaderDetails
          name={name}
          description={description}
          price={price}
          reviews={reviews}
        />
      </div>
    </div>
  );
};

export default ProductHeader;
