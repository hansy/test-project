// import StarSvg from "../Svg/star";
import GumroadSvg from "../Svg/gumroad";
import { getPercent } from "../../util/math";

const renderStars = (n, svgProps) => {
  const arr = [];

  for (let i = 0; i < n; i++) {
    // arr.push(<StarSvg key={i} {...svgProps} />);
    arr.push(<GumroadSvg key={i} {...svgProps} />);
  }

  return arr;
};

const STAR_SIZES = {
  sm: {
    width: 20,
    height: 20,
  },
  md: {
    width: 30,
    height: 30,
  },
  lg: {
    width: 40,
    height: 40,
  },
};

export const getStarSize = (size) => {
  return STAR_SIZES[size];
};

const StarRating = ({ rating, size = "md" }) => {
  const starSize = getStarSize(size);

  return (
    <div className="star-rating">
      <div className="star-rating__stars">
        <div
          id="averageRating"
          className="star-rating__stars--filled"
          style={{ width: `${getPercent(rating, 5)}%` }}
        >
          {/* {renderStars(5, starSize)} */}
          {renderStars(5, { ...starSize, opacity: "1" })}
        </div>
        <div className="star-rating__stars--empty">
          {/* {renderStars(5, starSize)} */}
          {renderStars(5, { ...starSize, opacity: "0.15" })}
        </div>
      </div>
    </div>
  );
};

export default StarRating;
