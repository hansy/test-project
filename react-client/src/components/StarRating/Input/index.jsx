import { Fragment } from "react";
// import StarSvg from "../../Svg/star";
import GumroadSvg from "../../Svg/gumroad";
import { getStarSize } from "../index";

// Render every star rating input from 0.5 to 5 (in 0.5 increments)
// Half stars are absolutely positioned over their full counterparts
const renderInputs = (svgProps) => {
  const arr = [];

  // Going to render 10 inputs in total to accommodate for half stars.
  // The highest input value is 5, the lowest is 0.5
  for (let i = 5; i > 0; i = i - 0.5) {
    const isInteger = Number.isInteger(i);

    arr.push(
      <Fragment key={i}>
        <input type="radio" id={`starRating${i}`} name="rating" value={i} />
        <label
          className={isInteger ? "full" : "half"}
          htmlFor={`starRating${i}`}
          title={`${i} stars`}
          style={{
            width: `${!isInteger && svgProps.width / 2 + "px"}`,
            left: `${!isInteger && svgProps.width * (i - 0.5) + "px"}`,
          }}
        >
          {/* <StarSvg {...svgProps} /> */}
          <GumroadSvg {...svgProps} />
        </label>
      </Fragment>
    );
  }

  return arr;
};

const StarRatingInput = ({ size = "md", onChange }) => {
  const starSize = getStarSize(size);

  return (
    <fieldset
      onChange={(e) => onChange(e.target.value)}
      className="star-rating__input"
    >
      {renderInputs(starSize)}
    </fieldset>
  );
};

export default StarRatingInput;
