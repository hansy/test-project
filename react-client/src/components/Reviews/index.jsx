import Review from "./Review";

const Reviews = ({ reviews = [] }) => {
  return (
    <div>
      {reviews.map((review) => {
        return (
          <Review
            key={review.id}
            rating={review.rating}
            description={review.description}
          />
        );
      })}
    </div>
  );
};

export default Reviews;
