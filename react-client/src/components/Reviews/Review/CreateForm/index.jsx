import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { trackPromise } from "react-promise-tracker";
import StarRatingInput from "../../../StarRating/Input";
import { createReview } from "../../../../api/reviews";

const schema = yup.object().shape({
  rating: yup
    .number()
    .positive()
    .min(0.5, "Please select a rating")
    .max(5, "Rating can't be greater than 5 stars")
    .required(),
  description: yup.string().required(),
  product_id: yup.number().integer().required(),
});

const CreateReviewForm = ({ productId, onCancel, onSuccess }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
    clearErrors,
  } = useForm({
    resolver: yupResolver(schema),
  });

  setValue("product_id", productId);

  const resetForm = () => {
    reset({
      rating: 0,
      description: "",
    });
  };

  const cancel = () => {
    onCancel();
    resetForm();
  };

  const onSubmit = async (data) => {
    try {
      const review = await trackPromise(createReview(data));
      onSuccess(review);
      resetForm();
    } catch {
      // handle error
    }
  };

  const setRating = (rating) => {
    clearErrors("rating");
    setValue("rating", rating, { shouldValidate: true });
  };

  return (
    <div>
      <h3 className="text-lg leading-6 font-medium text-gray-900">
        Add new review
      </h3>
      <form className="my-4" onSubmit={handleSubmit(onSubmit)}>
        <div className="my-2">
          <label className="block text-sm font-medium text-gray-700 mb-2">
            Select your rating
          </label>
          <StarRatingInput onChange={setRating} />
          <p className="mt-2 text-sm text-red-500">{errors.rating?.message}</p>
        </div>

        <div>
          <label
            htmlFor="description"
            className="block text-sm font-medium text-gray-700"
          >
            Description
          </label>
          <div className="mt-1">
            <textarea
              id="description"
              name="description"
              rows="3"
              className="shadow-sm block w-full sm:text-sm border border-gray-300 rounded-md p-2"
              {...register("description")}
            ></textarea>
          </div>
          <p className="mt-2 text-sm text-red-500">
            {errors.description?.message}
          </p>
        </div>
        <div className="pt-5">
          <div className="flex justify-end">
            <button
              type="button"
              className="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={cancel}
            >
              Cancel
            </button>
            <button
              type="submit"
              className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gumroad-green hover:gumroad-green focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gumroad-green"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default CreateReviewForm;
