import StarRating from "../../StarRating";

const Review = ({ id, rating, description, createdAt }) => {
  return (
    <div
      key={id}
      className="flex text-sm text-gray-500 my-4 max-w-md border-b border-gray-300 pb-3"
    >
      <div>
        <p>{createdAt}</p>

        <div className="flex items-center">
          <StarRating size="sm" rating={rating} />
          <p className="text-xs ml-2 text-gray-500">{rating} / 5</p>
        </div>

        <div
          className="mt-4 prose prose-sm max-w-none text-gray-500"
          dangerouslySetInnerHTML={{ __html: description }}
        />
      </div>
    </div>
  );
};

export default Review;
