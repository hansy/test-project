import { usePromiseTracker } from "react-promise-tracker";
import AnimatedCircleSvg from "../Svg/animated-circle";

const Loader = () => {
  const { promiseInProgress } = usePromiseTracker();

  if (promiseInProgress) {
    return (
      <div className="loader">
        <AnimatedCircleSvg width="100" height="100" stroke="white" />
      </div>
    );
  }
  return null;
};

export default Loader;
