# Gumroad Coding Challenge - MVP

[View app](https://hansy.gitlab.io/test-project/)

_Note: Data may take a couple seconds to load as backend server needs to "wake up"._

This is a simple static page made with HTML, CSS, and jQuery. The page only displays one product (product ID hardcoded in `scripts.js`). Reviews can be created for the product. All data is fetched from and sent to a backend server (code [here](https://gitlab.com/hansy/test-project/-/tree/main/server)).

The only dependancy is jQuery (asynchronously loaded in page from CDN). Styling is bare minumum CSS (no framework used).
