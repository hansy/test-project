# Gumroad Coding Challenge

This project contains three codebases:

1. A static HTML MVP

- Repo: https://gitlab.com/hansy/test-project/-/tree/main/client
- Demo URL: https://hansy.gitlab.io/test-project/

2. A simple, Ruby server

- Repo: https://gitlab.com/hansy/test-project/-/tree/main/server

3. A refactored React app

- Repo: https://gitlab.com/hansy/test-project/-/tree/main/react-client
- Demo URL: https://gumroad-coding-challenge-v2.vercel.app

The technologies used were chosen to closely mimick Gumroad's tech stack. More information about each codebase can be found in the README of their respective directories.

I'm not sure I would do anything too different if I had to redo the project. The only thing that comes to mind is building out the real-time client updates without relying on a 3rd-party service (Pusher).
