require 'dotenv/load'
require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/cross_origin'
require 'pusher'
require 'json'

require './models/product'
require './models/review'

pusher = Pusher::Client.new(
  app_id: ENV['PUSHER_APP_ID'],
  key: ENV['PUSHER_KEY'],
  secret: ENV['PUSHER_SECRET'],
  cluster: 'us2',
  use_tls: true
)

configure do
  enable :cross_origin
end

before do
  response.headers['Access-Control-Allow-Origin'] = '*'
end

options "*" do
  response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
  response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
  response.headers["Access-Control-Allow-Origin"] = "*"

  200
end

get '/products/:id' do
  @product = Product.includes(:reviews).find_by_id(params[:id])

  @product ? @product.to_json(include: [:reviews]) : 404
end

get '/products' do
  @products = Product.all

  @products.to_json
end

post '/reviews' do
  request.body.rewind
  data = JSON.parse request.body.read

  @review = Review.create(data)
  pusher.trigger('products', 'review', @review)

  content_type :json

  return @review.to_json
end
