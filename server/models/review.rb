class Review < ActiveRecord::Base
  belongs_to :product
  
  validates :product_id, presence: true
  validates :rating, numericality: {
    greater_than_or_equal_to: 0.5,
    less_than_or_equal_to: 5
  }, presence: true
  validates :description, presence: true

  default_scope { order('created_at DESC') }
end
