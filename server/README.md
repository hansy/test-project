# Gumroad Coding Challenge - Backend

This is the backend for the project. It's a Sinatra (Ruby) app using ActiveRecord as the ORM to store data in a MySQL DB.

## Running locally

### Requirements

- Ruby v2.6.3
- Bundler v2.2.15
- MySQL v14.14
- [Pusher](https://pusher.com) account

### Environment variables

You'll need to create a `.env` file in the root of the project.

```bash
$ cp .env.example .env
```

Replace the stubbed Pusher environment variables with the proper keys/secrets from your Pusher account.

### Installation

```ruby
# install dependencies
$ gem install

# create DB (after ensuring MySQL is running)
# may need to update socket connection in config/database.yml depending
# on your MySQL installation
# In mysql console, get socket path via: select @@socket;
$ rake db:create

# run migrations
$ rake db:migrate

# seed DB
$ gem install faker
$ rake db:seed

# run server
$ ruby index.rb
```

## Deployment

The easiest deployment target is Heroku.

### Requirements

- Heroku CLI

To setup MySQL in Heroku, install the ClearDB MySQL add-on (https://elements.heroku.com/addons/cleardb). Because this project uses the `mysql2` MySQL adapter, you'll need to update the adapter in the `CLEARDB_DATABASE_URL` env var Heroku creates.

To do so, in the Herkou dashboard, navigate to **Settings > Reveal Config Vars**.

Edit the `CLEARDB_DATABASE_URL` env var by changing the adapter from "mysql" to "mysql2" (the first part of the URL connection string).

```bash
# add Heroku remote
$ heroku git:remote -a <name of Heroku app>

# deploy to Heroku
$ git push heroku master

# setup Heroku DB
$ heroku run rake db:create && heroku run rake db:migrate

# optionally seed DB
$ heroku run rake db:seed
```
